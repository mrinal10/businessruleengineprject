import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class test{

    private static List<String> regexCheck(String s){
        String[] arrayS = s.split(" ");
        List<String> list = new ArrayList<>();
        for(int i=0; i<arrayS.length; i++){
            if(check(arrayS[i])){
                list.add(arrayS[i]);
            }
        }
        return list;
    }

    private static boolean check(String s){
        String regexStartCaps = "\\b([A-Z])(\\S*?)\\b";
        String containsNumber = "(.)*(\\d)(.)*";
        if(getPatternMatch(regexStartCaps, s) 
                && (getPatternMatch(containsNumber, s) || s.contains("_")) ){
            return true;
        }
        return false;
    }

    private static boolean getPatternMatch(String patternString, String text){        
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static void main(String[] args) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in, StandardCharsets.UTF_8);
        BufferedReader in = new BufferedReader(reader);
        String line;
        while ((line = in.readLine()) != null) {
          List<String> output = regexCheck(line);
          Iterator<String> itr = output.iterator();
          while(itr.hasNext()){
              System.out.println(itr.next());
          }
        }        
      }
}