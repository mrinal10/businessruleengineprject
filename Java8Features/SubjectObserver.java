@FunctionalInterface
public interface SubjectObserver {

    void update();

}