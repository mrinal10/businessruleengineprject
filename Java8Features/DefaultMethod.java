interface oldInterface {
    public void existingMethod();

    default public void newDefaultMethod() {
        System.out.println("New default method is added in interface");
    }
}

class oldInterfaceImpl implements oldInterface {

    @Override
    public void existingMethod() {
        // TODO Auto-generated method stub

    }

}

class testClass {
    public static void main(String[] args) {
        oldInterfaceImpl obj = new oldInterfaceImpl();
        // print “New default method add in interface”
        obj.newDefaultMethod();
    }
}