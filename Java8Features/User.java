import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private List<SubjectObserver> observers = new ArrayList<>();

    public User(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public void addObserver(SubjectObserver observer) {
        observers.add(observer);
    }

    public void deleteObserver(SubjectObserver observer) {
        observers.remove(observer);
    }

    private void notifyObservers() {
        observers.stream().forEach(observer -> observer.update());
    }
}

class TestObserver{
    public static void main(String[] args) {
        User user = new User("John");
        user.addObserver(() -> 
            System.out.println("Observable subject " + user.getName() + " has changed its state."));
        user.setName("Jack");
    }
}