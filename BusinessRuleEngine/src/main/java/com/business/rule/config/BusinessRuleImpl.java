package com.business.rule.config;

import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.yaml.snakeyaml.Yaml;

public class BusinessRuleImpl implements BusinessRule {

	@Autowired
	RuleProcessor ruleProcessor;

	@Override
	public void generateShippingSlip() {
		System.out.println(" Shipping Slip generated ");
	}

	@Override
	public void generateDuplicatePackingSlipRoaylty() {
		System.out.println("Duplicate Packing Slip for royalty generated ");

	}

	@Override
	public void giveAgentCommision() {
		System.out.println("Commision Provided");

	}

	@Override
	public void applyUpgrade() {
		System.out.println("Upgrade applied");

	}

	@Override
	public void sendEmail() {
		System.out.println("Email send to user");

	}

	@Override
	public void askVideoName() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Insert Video name : ");
		String name = sc.next();
		Map<String, String> mapp = (Map<String, String>) readYaml(name);
		if (mapp != null && mapp.keySet().contains(name)) {
			System.out.println("Free video provided : " + mapp.get(name));
		}
		sc.close();
	}

	private Object readYaml(String key) {
		Yaml yaml = new Yaml();
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("application.yml");
		Map<String, Object> objectMap = yaml.load(inputStream);
		return objectMap.get(key);
	}

	@Override
	public void provideVideo() {
		System.out.println("video Provided");		
	}

}
