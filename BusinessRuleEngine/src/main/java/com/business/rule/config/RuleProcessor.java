package com.business.rule.config;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Component
public class RuleProcessor {

	public void processReq(String product) throws NoSuchMethodException, 
						SecurityException, IllegalAccessException, 
								IllegalArgumentException, InvocationTargetException {
		Map<String, String> map = getConfigOfProduct(product);
		if(map == null) {
;			return;
		}
		for(Entry<String, String> entry : map.entrySet()) {
			operate(entry.getKey());
		}
	}

	public void operate(String key) throws NoSuchMethodException, SecurityException, 
					IllegalAccessException, IllegalArgumentException,
							InvocationTargetException {
		Method m = BusinessRuleImpl.class.getMethod(this.getValue(key));
		m.invoke(new BusinessRuleImpl(), null);
	}
	
	public String getValue(String s) {
		try {
			return (String)readYaml(s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public Map<String, String> getConfigOfProduct(String productName) {
		Map<String, String> configMap = new HashMap<>();
		try {
			return (Map<String, String>)readYaml(productName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return configMap;
	}

	private Object readYaml(String key) throws JsonParseException, 
						JsonMappingException, IOException {
		Yaml yaml = new Yaml();
		InputStream inputStream = this.getClass()
		  .getClassLoader()
		  .getResourceAsStream("application.yml");
		Map<String, Object> objectMap = yaml.load(inputStream);		
		return objectMap.get(key);
	}
}
