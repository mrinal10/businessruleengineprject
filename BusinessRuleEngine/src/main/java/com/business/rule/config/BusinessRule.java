package com.business.rule.config;

public interface BusinessRule {
	
	public void generateShippingSlip();
	public void generateDuplicatePackingSlipRoaylty();
	public void giveAgentCommision();
	public void applyUpgrade();
	public void sendEmail();
	public void askVideoName();
	public void provideVideo();
}
