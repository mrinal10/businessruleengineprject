package com.business.rule;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.business.rule.config.RuleProcessor;
import com.business.rule.util.ConfigUtil;

@Component
public class MyRunner implements CommandLineRunner {

	@Autowired
	ConfigUtil util;
	
	@Autowired
	RuleProcessor ruleProcessor;
	
	@Override
	public void run(String... args) throws Exception {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("products available : ");
			for(String productName: util.loadDetails()) {
				System.out.println(productName);
			}
			Set<String> set = new HashSet<>(); 
	        for (String t : util.loadDetails()) { 
	            set.add(t); 
	        } 
			System.out.print("Enter the product : ");
			String product = sc.next();

	        System.out.println("*****************************Actions on Product**************************************");
	        if(!set.contains(product)) {
	        	System.out.println("No Such Product");
	        }else
	        	ruleProcessor.processReq(product);
	        System.out.println("************************ one iteration Done *****************************");
	        System.out.println("*************************************************************************");
	        System.out.println();
	        System.out.println();
		}
	}
}