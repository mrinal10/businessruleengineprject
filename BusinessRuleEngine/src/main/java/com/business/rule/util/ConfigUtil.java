package com.business.rule.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class ConfigUtil {

	@Value("${products_available}")
	String[] availableProducts;

	@Bean
	public String[] loadDetails() {
		return availableProducts;
	}
}
