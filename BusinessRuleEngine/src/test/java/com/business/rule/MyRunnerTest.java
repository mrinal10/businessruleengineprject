package com.business.rule;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.business.rule.config.RuleProcessor;
import com.business.rule.util.ConfigUtil;


@RunWith(MockitoJUnitRunner.class)
public class MyRunnerTest {
	
	@InjectMocks
	private MyRunner myRunner;
	
	@InjectMocks
	private ConfigUtil configUtil;
	
	@InjectMocks
	private RuleProcessor rulePro;
	
	@Before
	public void setup() {
		myRunner.ruleProcessor = rulePro;
		myRunner.util = configUtil;
	}
	
	@Test
	public void runnerTest() {
		try {
			myRunner.run("test");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
