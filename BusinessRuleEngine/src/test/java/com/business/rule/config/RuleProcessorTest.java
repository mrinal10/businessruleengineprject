package com.business.rule.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RuleProcessorTest {
	
	@InjectMocks
	private RuleProcessor ruleprocessor;
	
	
	@Test
	public void processReqTestForProduct_product() {
		try {
			ruleprocessor.processReq("product");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void processReqTestForProduct_dummy() {
		try {
			ruleprocessor.processReq("abc");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void processReqTestForProduct_book() {
		try {
			ruleprocessor.processReq("book");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void processReqTestForProduct_membership() {
		try {
			ruleprocessor.processReq("membership");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void processReqTestForProduct_membership_upgrade() {
		try {
			ruleprocessor.processReq("membership_upgrade");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void processReqTestForProduct_video() {
		try {
			ruleprocessor.processReq("video");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
